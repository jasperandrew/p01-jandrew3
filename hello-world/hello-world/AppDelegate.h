//
//  AppDelegate.h
//  hello-world
//
//  Created by Jasper Andrew on 1/19/17.
//  Copyright © 2017 Hello World, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

