//
//  ViewController.m
//  hello-world
//
//  Created by Jasper Andrew on 1/19/17.
//  Copyright © 2017 Hello World, Inc. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

int count = 0;

@implementation ViewController

@synthesize upLabel;

-(void)viewDidLoad {
    [super viewDidLoad];
}

-(void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(IBAction)countUp:(id)sender {
    count++;
    [upLabel setText:[NSString stringWithFormat:@"%d", count]];
    
    switch (count) {
        case 11: [sender setTitle:@"i am a button" forState:UIControlStateNormal]; break;
        case 17: [sender setTitle:@"how are you doing?" forState:UIControlStateNormal]; break;
        case 25: [sender setTitle:@"it's a shame we can't communicate" forState:UIControlStateNormal]; break;
        case 30: [sender setTitle:@"if only we had The Answer" forState:UIControlStateNormal]; break;
        case 33: [sender setTitle:@"to Life" forState:UIControlStateNormal]; break;
        case 38: [sender setTitle:@"the Universe..." forState:UIControlStateNormal]; break;
        case 42: [sender setTitle:@"...and Everything" forState:UIControlStateNormal]; break;
        case 69: [sender setTitle:@"*giggles*" forState:UIControlStateNormal]; break;
        case 77: [sender setTitle:@"ah, well" forState:UIControlStateNormal]; break;
        case 87: [sender setTitle:@"let's talk about you" forState:UIControlStateNormal]; break;
        case 99: [sender setTitle:@"how's the fam?" forState:UIControlStateNormal]; break;
        case 110: [sender setTitle:@"i mean, besides your mom" forState:UIControlStateNormal]; break;
        case 117: [sender setTitle:@"i know how she's doin'" forState:UIControlStateNormal]; break;
        case 123: [sender setTitle:@"🙋😎😱👏💪" forState:UIControlStateNormal]; break;
        case 140: [sender setTitle:@"i apologize" forState:UIControlStateNormal]; break;
        case 150: [sender setTitle:@"that was unnecessary" forState:UIControlStateNormal]; break;
        case 170: [sender setTitle:@"hey, c'mon" forState:UIControlStateNormal]; break;
        case 180: [sender setTitle:@"i said i was sorry" forState:UIControlStateNormal]; break;
        case 200: [sender setTitle:@"alright, i see how it is" forState:UIControlStateNormal]; break;
        case 210: [sender setTitle:@"i make a joke, and you ignore me" forState:UIControlStateNormal]; break;
        case 220: [sender setTitle:@"well guess what, buddy" forState:UIControlStateNormal]; break;
        case 230: [sender setTitle:@"two can play at this game" forState:UIControlStateNormal]; break;
        case 240: [sender setTitle:@"🤐" forState:UIControlStateNormal]; break;
        default:
            break;
    }
}

@end
