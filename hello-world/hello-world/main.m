//
//  main.m
//  hello-world
//
//  Created by Jasper Andrew on 1/19/17.
//  Copyright © 2017 Hello World, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
