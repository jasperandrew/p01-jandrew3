//
//  ViewController.h
//  hello-world
//
//  Created by Jasper Andrew on 1/19/17.
//  Copyright © 2017 Hello World, Inc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController

@property (nonatomic, strong) IBOutlet UILabel *upLabel;
@property (nonatomic, strong) IBOutlet UIButton *upButton;

- (IBAction)countUp:(id)sender;

@end
